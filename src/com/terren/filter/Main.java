package com.terren.filter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.terren.filter.config.Config;
import com.terren.filter.entity.OpinionAnalysis;
import com.terren.filter.jdbc.CRUD;

public class Main {

	public static void main(String[] args) {
		while(true) {
			try {
				CRUD crud = new CRUD();
				List<String> strList = crud.selectKeyword();//关键词
				Set<String> set = filterKeyword(strList);//查询结果
				List<OpinionAnalysis> opinions = crud.selectOpinions();
				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Config.LIMIT_TIME = sdf.format(date);
				for(OpinionAnalysis o : opinions) {
					if(!findKeyword(o,set)) {
						String urlId = o.getUrlid();
						System.out.println("更新不符合要求的内容" + urlId);
						crud.update(urlId);
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				try {
					System.out.println("暂停一分钟...");
					Thread.sleep(60000);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
	}
		
	

	private static Set<String> filterKeyword(List<String> strList){
		Set<String> sets = new HashSet<String>();
		for(String str : strList) {
			if(str.contains(" ")) {
				String[] temp = str.split(" ");
				for(String tempStr : temp) {
					if(!tempStr.contains("借点钱")) {
						sets.add(tempStr);
					}
				}
			}	
		}
		return sets;
	}
	
	private static boolean findKeyword(OpinionAnalysis opinion,Set<String> set) {
		boolean flag = false;
		Iterator<String> urlIt = set.iterator();
		while(urlIt.hasNext()) {
			if(opinion.getContent().contains(urlIt.next())) {
				flag = true;
				break;
			}
			
		}
		if(flag) {
			System.out.println("--------------符合要求--------------");
		}
		return flag;
	}
}
