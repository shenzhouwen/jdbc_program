package com.terren.filter.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBCHelper {
	
	public static Connection getConn(){
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://192.168.20.8:4000/newsdb?characterEncoding=UTF-8&useUnicode=true&zeroDateTimeBehavior=convertToNull&allowMultiQueries=true";
			String username = "websis";
			String password = "websis";
			url += "&user=" + username + "&password=" + password;
			// System.out.println(url);
			conn = DriverManager.getConnection(url);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public static void closeAll(ResultSet rs, PreparedStatement prepStmt, Connection conn) {
		try {
			if(rs != null){
				rs.close();
			}
			if(prepStmt != null){
				prepStmt.close();
			}
			if(conn != null){
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	public static void closeConn(Connection conn) {
		try {
			if(conn != null){
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void closePrepStmt(PreparedStatement prepStmt) {
		try {
			if(prepStmt != null){
				prepStmt.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void closeRs(ResultSet rs) {
		try {
			if(rs != null){
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
