package com.terren.filter.entity;

public class OpinionAnalysis {
	private String urlid; // URL的64位哈希
	private String url = "";// URL地址
	private Integer limit;
	private int opinion_analysis;//舆情指数分析 0:未分析 1:已分析 -1:分析错误
	
	private String title = ""; // 文章标题
	private String summary = ""; // 文章摘要
	private String content = ""; // 文章内容
	private String author = ""; // 文章发布作者
	private String pubtime = "0000-00-00"; // 文章的发布时间
	private String newssource = ""; // 文章的新闻来源
	private int opinion;//舆情指数分析 0:未分析 1:已分析 -1:分析错误
	private String keyword;
	
	public String getUrlid() {
		return urlid;
	}
	public void setUrlid(String urlid) {
		this.urlid = urlid;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public int getOpinion_analysis() {
		return opinion_analysis;
	}
	public void setOpinion_analysis(int opinion_analysis) {
		this.opinion_analysis = opinion_analysis;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPubtime() {
		return pubtime;
	}
	public void setPubtime(String pubtime) {
		this.pubtime = pubtime;
	}
	public String getNewssource() {
		return newssource;
	}
	public void setNewssource(String newssource) {
		this.newssource = newssource;
	}
	public int getOpinion() {
		return opinion;
	}
	public void setOpinion(int opinion) {
		this.opinion = opinion;
	}
	
	
}
